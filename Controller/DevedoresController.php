<?php
include_once("Model/Devedores/DevedoresModel.php");
include_once("Model/Devedores/DevedoresVO.php");
include_once("Model/Devedores/DevedoresDAO.php");

class DevedoresController
{

  function __construct()
  {

  }

  public function DevedoresController(){

  }

  public function list(){
    $model = new DevedoresModel();
    $_SESSION["devedores"] = $model->getAllModel();
    include("View/devedores/list.php");
  }

  public function save(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setNome($_POST["txtNome"]);
    $vo->setcpf_cnpj($_POST["txtCpf_cnpj"]);
    $vo->setnascimento($_POST["txtNascimento"]);

    if($model->insertModel($vo)){
      $_SESSION["msg"] = "Produto cadastrado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao cadastrar o produto";
    }

    header("Location: list");
  }

  public function new(){
    include("View/devedores/insert.php");
  }

  public function edit(){
    $model = new DevedoresModel();
    $vo = $model->getByIdModel($_GET["id"]);
    $_SESSION["id"] = $vo->getId();
    $_SESSION["name"] = $vo->getNome();
    $_SESSION["cpf_cnpj"] = $vo->getCpf_cnpj();
    $_SESSION["nascimento"] = $vo->getNascimento();

    include("View/devedores/edit.php");
  }

  public function update(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setId($_POST["txtId"]);
    $vo->setNome($_POST["txtNome"]);
    $vo->setcpf_cnpj($_POST["txtCpf_cnpj"]);
    $vo->setnascimento($_POST["txtNascimento"]);

    if($model->updateModel($vo)){
      $_SESSION["msg"] = "Produto atualizado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao atualizar o produto";
    }

    header("Location: ".$_SESSION["base_url"]."devedores/list");
  }

  public function delete(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setId($_GET["id"]);

    if($model->deleteModel($vo)){
      $_SESSION["msg"] = "Produto atualizado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao atualizar o produto";
    }

    header("Location: ".$_SESSION["base_url"]."devedores/list");
  }

  public function __destruct(){
    // return self::getInstance()->close();
  }
}

 ?>
