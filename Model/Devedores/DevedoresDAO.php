<?php
include_once 'Model/DB.php';
include_once 'Model/Required.php';

class DevedoresDAO implements Required{
  protected $table = "devedores";

  public function getAll(){
    $sql = "SELECT * FROM $this->table WHERE deleted_at IS NULL";
    $stmt = DB::prepare($sql);
    $query = $stmt->execute();

    return $stmt->fetchAll();
  }

  public function getById($id){
    $sql = "SELECT * FROM $this->table where id = :id limit 1";
    $stmt = DB::prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $query = $stmt->execute();

    $vo = new DevedoresVO();

    while ($reg = $stmt->fetch()){
      $vo->setId($reg->id);
      $vo->setNome($reg->nome);
      $vo->setCpf_cnpj($reg->cpf_cnpj);
      $vo->setNascimento($reg->nascimento);
    }

    return $vo;
  }

  public function insert($value){
    $sql = "INSERT INTO $this->table (nome, cpf_cnpj, nascimento) VALUES (:nome, :cpf_cnpj, :nascimento)";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':nome', $value->getNome());
    $stmt->bindValue(':cpf_cnpj', $value->getCpf_cnpj());
    $stmt->bindValue(':nascimento', $value->getNascimento());

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

  public function update($value){
    $sql = "UPDATE $this->table set nome = :nome, cpf_cnpj = :cpf_cnpj, nascimento = :nascimento where id = :id";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':nome', $value->getNome());
    $stmt->bindValue(':cpf_cnpj', $value->getCpf_cnpj());
    $stmt->bindValue(':nascimento', $value->getNascimento());
    $stmt->bindValue(':id', $value->getId());

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

  public function delete($value){
    $now = date('Y-m-d h:m:s');

    $sql = "UPDATE $this->table set deleted_at = :deleted_at where id = :id";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':id', $value->getId());
    $stmt->bindValue(':deleted_at', $now);

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

}

 ?>
