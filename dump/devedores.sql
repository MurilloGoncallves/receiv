-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 15, 2022 at 02:34 AM
-- Server version: 8.0.21
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `receiv`
--

-- --------------------------------------------------------

--
-- Table structure for table `devedores`
--

DROP TABLE IF EXISTS `devedores`;
CREATE TABLE IF NOT EXISTS `devedores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(200)  NOT NULL,
  `cpf_cnpj` varchar(200) NOT NULL,
  `nascimento` date NOT NULL,
  `logradouro` text NOT NULL,
  `numero` varchar(10) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `devedores`
--

INSERT INTO `devedores` (`id`, `nome`, `cpf_cnpj`, `nascimento`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 'Notebook Samsung 1', 'Samsung', '0000-00-00', '', '', '', '', '', '', '2022-01-08 16:13:50', '2021-10-07 21:42:16', '2022-01-08 07:01:50'),
(2, 'Lucas', 'Adidas', '2011-00-00', '', '', '', '', '', '', '2022-01-08 19:50:35', '2022-01-08 14:22:32', '2022-01-15 02:01:51'),
(3, 'Calça', 'Adidas', '0000-00-00', '', '', '', '', '', '', '2022-01-08 14:29:18', '2022-01-08 14:29:18', '2022-01-15 02:01:48'),
(4, 'Shorts', 'Adidas', '0000-00-00', '', '', '', '', '', '', '2022-01-08 19:50:03', '2022-01-08 16:17:42', '2022-01-08 10:01:03'),
(5, 'Camiseta Azul', 'Adidas', '0000-00-00', '', '', '', '', '', '', '2022-01-08 19:48:22', '2022-01-08 16:18:16', '2022-01-08 10:01:22'),
(6, 'Moleton', 'Corner', '2018-00-00', '', '', '', '', '', '', '2022-01-08 19:48:03', '2022-01-08 19:46:31', '2022-01-08 10:01:03'),
(7, 'Moleton Exclusive', 'Corner', '2027-00-00', '', '', '', '', '', '', '2022-01-08 19:47:54', '2022-01-08 19:46:53', '2022-01-08 10:01:54'),
(8, 'Murilo', '000.000.000-00', '2021-12-27', '', '', '', '', '', '', '2022-01-14 23:12:28', '2022-01-14 23:12:28', '2022-01-15 02:01:41'),
(9, 'Murilo', '000.000.000-00', '2022-01-20', '', '', '', '', '', '', '2022-01-14 23:23:55', '2022-01-14 23:23:55', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
