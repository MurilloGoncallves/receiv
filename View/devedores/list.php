<?php include_once 'View/elements/header.php'; ?>
  <h2 class="text-center">Lista de devedores</h2>
  <a href="<?php echo $_SESSION["base_url"]; ?>devedores/new">Inserir um novo</a>
  <table style="border: 1px solid #000; width: 100%;">
    <tr>
      <td>Id</td>
      <td>Nome</td>
      <td>CPF / CNPJ</td>
      <td>Nascimento</td>
      <td>Última alteração</td>
      <td></td>
    </tr>
    <?php foreach ($_SESSION["devedores"] as $row): ?>
      <tr>
        <td><?php echo $row->id; ?></td>
        <td><?php echo $row->nome; ?></td>
        <td><?php echo $row->cpf_cnpj; ?></td>
        <td><?php echo $row->nascimento; ?></td>
        <td><?php echo $row->updated_at; ?></td>
        <td>
          <a href="<?php echo $_SESSION["base_url"]; ?>devedores/edit/<?php echo $row->id; ?>">Editar</a>
          <a href="<?php echo $_SESSION["base_url"]; ?>devedores/delete/<?php echo $row->id; ?>">Deletar</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </table>
<?php include_once 'View/elements/footer.php'; ?>
